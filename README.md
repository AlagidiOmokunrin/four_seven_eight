# four_seven_eight

My #FlutterCreate Submission

## Motivation
My app guides people through a breathing exercise that is known to help with anxiety and some has said it helps them fall sleep    
- [Video of the creator showing how it is done](https://www.youtube.com/watch?v=YRPh_GaiL8s)     
- ![four seven eight image](four_seven_eight_info.png)

I chose to implement this because I wanted to explore flutter's animation and the i8n capabilities, two areas of flutter I have always been interested, but never touched        
I picked the Arabic locale because it is RTL as well as a different language, I'm sure Arabic speaking people have anxiety and sleeping problems just like everybody     
*Full Disclosure* I don't speak Arabic.   
    
## Environment  
The app has internalization which requires [iOS App Bundle updating](https://flutter.dev/docs/development/accessibility-and-localization/internationalization#appendix-updating-the-ios-app-bundle)    
I don't have a Fully-loaded Apple iMac Pro with 256GB RAM worth 10,000 to test it on so this is only tested on Android    
***cough*** ***cough***   

## Instruction
There are two FAB, A large play button and a small language button
-  The play button starts the animation
-  The language button toggles the locale between English and Arabic


The animation consist of three phases for 19 seconds    
"Inhale": 4 seconds    
"Hold": 7 seconds    
"Exhale": 8 seconds (Breathe through your mouth, making a whooshing sound; *Don't rush this part*)    
    
There are instructions in the middle of the screen to guide the users.    
The animations of the app mimic the motions of breathing to add another level of guidance.    

## Leaving Notes
This contest was a very exciting challenge for me.   
This was harder to implement than I thought it would be, especially with the maximum file size constraint
I had to scour the animation and i8n docs to make sure I was getting the best value with the least amount of code.  
I can absolutely say I am a better Flutter developer because of this contest.    
Thank you very much!
