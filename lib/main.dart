import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(EasyLocalization(child: MA()));

class MA extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
        data: data,
        child: MaterialApp(localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          EasylocaLizationDelegate(
              locale: data.locale ?? Locale('en'), path: 'resources/lang')
        ], supportedLocales: [
          Locale('en'),
          Locale('ar')
        ], locale: data.locale, home: BE()));
  }
}

class BE extends StatefulWidget {
  @override
  State createState() => BES();
}

class BES extends State<BE> with SingleTickerProviderStateMixin {
  AnimationController _ac;
  Animation<double> _icS;
  Animation<Color> _ocC;
  Animation<String> _itT;
  int _tT = 4, _cT;
  bool _en = true;
  BuildContext _sC;

  @override
  void initState() {
    super.initState();
    double fS = 0.21, sS = 0.37, tS = 0.42;
    _ac =
        AnimationController(duration: const Duration(seconds: 19), vsync: this);
    _ac.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        if (++_cT <= _tT) {
          _ac.forward(from: 0);
        } else {
          Scaffold.of(_sC).showSnackBar(
              SnackBar(content: Text(AppLocalizations.of(_sC).tr("complete"))));
        }
      } else if (status == AnimationStatus.forward) {
        Scaffold.of(_sC).removeCurrentSnackBar();
        Scaffold.of(_sC).showSnackBar(SnackBar(
            content: Text(AppLocalizations.of(_sC)
                .tr("status", args: [_cT.toString(), _tT.toString()]))));
      }
    });
    _icS = TweenSequence(<TweenSequenceItem>[
      _tweenSF(0.0, 250.0, fS),
      _cTweenSF(250.0, sS),
      _tweenSF(250.0, 0.0, tS)
    ]).animate(_ac);
    _ocC = TweenSequence(<TweenSequenceItem>[
      TweenSequenceItem(tween: ConstantTween(Colors.blueAccent), weight: fS),
      TweenSequenceItem(
          tween: ColorTween(begin: Colors.blueAccent, end: Colors.white),
          weight: sS),
      TweenSequenceItem(
          tween: ColorTween(begin: Colors.white, end: Colors.blueAccent),
          weight: tS)
    ]).animate(_ac);
    _itT = TweenSequence(<TweenSequenceItem>[
      _cTweenSF("inhale", fS),
      _cTweenSF("hold", sS),
      _cTweenSF("exhale", tS)
    ]).animate(_ac);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(AppLocalizations.of(context).tr("title")),
            backgroundColor: Colors.blueAccent),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FloatingActionButton(
                onPressed: () {
                  setState(() {
                    var data = EasyLocalizationProvider.of(context).data;
                    _en
                        ? data.changeLocale(Locale("ar"))
                        : data.changeLocale(Locale("en"));
                    _en = !_en;
                    Scaffold.of(_sC).removeCurrentSnackBar();
                  });
                },
                backgroundColor: Colors.blue,
                child: new Icon(Icons.language),
                mini: true),
            FloatingActionButton(
                onPressed: () {
                  _pA();
                },
                backgroundColor: Colors.blue,
                child: new Icon(Icons.play_arrow))
          ],
        ),
        body: new Builder(builder: (BuildContext context) {
          _sC = context;
          return AnimatedBuilder(animation: _ac, builder: _buildAnimation);
        }));
  }

  Stack _buildAnimation(BuildContext context, Widget child) {
    return Stack(
      children: <Widget>[
        Center(child: Container(color: _ocC.value)),
        Center(
            child: Container(
                width: _icS.value,
                height: _icS.value,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(360)))),
        Center(
            child: Text(AppLocalizations.of(context).tr(_itT.value),
                style: TextStyle(color: Colors.blueAccent, fontSize: 30)))
      ],
    );
  }

  void _pA() {
    _cT = 1;
    _ac.forward(from: 0);
  }

  @override
  void dispose() {
    _ac.dispose();
    super.dispose();
  }

  TweenSequenceItem<T> _tweenSF<T>(T b, T e, double w) {
    return TweenSequenceItem<T>(tween: Tween<T>(begin: b, end: e), weight: w);
  }

  TweenSequenceItem<T> _cTweenSF<T>(T c, double w) {
    return TweenSequenceItem<T>(tween: ConstantTween<T>(c), weight: w);
  }
}
